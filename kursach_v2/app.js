const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const config = require('./config');
const PORT = config.ServerPort;
const app = express();
const passport = require('passport');
const session = require('express-session');
const databaseUrl = config.DatabaseUrl;
const connectOptions = { useNewUrlParser : true };
//const mustache = require("mustache");
const mustacheExpress = require('mustache-express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
app.use(cookieParser());
app.use(session({
	secret: "secret",
	resave: false,
	saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());



//login
// view engine setup
//app.engine('html', consolidate.swig);
app.engine('mst', mustacheExpress());
app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'html');
app.set('view engine', 'mst');
//app.register(".mustache", require('stache'));
app.use(bodyParser.urlencoded({ extended: true }));
//app.use(busboyBodyParser());
//app.use(favicon(__dirname + '/public/images/favicon.ico'));



mongoose.connect(databaseUrl,connectOptions)
  .then(() => console.log(`Database connected: ${databaseUrl}`))
  .then(() => app.listen(PORT,() => console.log(`Server started: ${PORT}`)))
  .catch(err => console.log(`Start error: ${err}`))

  //const User = require("./models/user");





  
//  app.get('/auth/register', function(req, res) {
//     res.render('register',{"title":"Registration"});
//   });
//   app.post('/auth/register', (req, res) => {
//     let login = req.body.login;
//     console.log(req.body);
//     // if (User.isUniqueUsername(login)) {
//     //     res.redirect('/auth/register?error=Username+already+exists');
//     //     return;
//     // }
//     res.redirect('../../');
//   });
  

  const userRouter = require("./routes/user");
  app.use('/users',userRouter);

  const botRouter = require("./routes/bot");
  app.use('/bots',botRouter);

  const collectionRouter = require("./routes/collection");
  app.use('/collections',collectionRouter);

  const apiRouter = require("./routes/api");
  app.use('/api/v1/',apiRouter);

  const developerRouter = require("./routes/developer");
  app.use('/developer/v1/',developerRouter);


  const authRouter = require("./routes/auth");
  app.use('/auth', authRouter);

  const profileRouter = require("./routes/profile");
  app.use('/profile', profileRouter);

  app.use('/', express.Router().get('/', function(req, res) {
    res.render('index',{"menuHome":"currentpage","title":"Telegram bots",user : req.user});
  }));

  


  app.use('/about', function(req, res) {
    res.render('about',{"menuAbout":"currentpage","title":"Telegram bots",user : req.user});
  });

  

 

    




  
    
