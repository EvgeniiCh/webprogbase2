const express = require('express');

const router = express.Router();

const support = require('../models/crypto');
const passport = require('../models/passport');

const fileupload = require("express-fileupload");
router.use(fileupload({}));

const config = require('../config');

const cloudinary = require('cloudinary');
cloudinary.config({
    cloud_name: config.cloudinary.cloud_name,
    api_key: config.cloudinary.api_key,
    api_secret: config.cloudinary.api_secret
});


const User = require("../models/user");

router.post('/register', (req, res) => {
  let login = req.body.login;
  let password = req.body.password;
  let confirmPassword = req.body.confirmPassword;
  let fullname = req.body.fullname;
  let bio =req.body.bio;

  //console.log(req.body);
  if (!User.isUniqueUsername(login)) {
      res.redirect('/auth/register?error=Username+already+exists');
      return;
  }
    if (password !== confirmPassword) {
        res.redirect('/auth/register?error=Passwords+do+not+match');
        return;
    }
    const fileObject = req.files.img;
    let fileBuffer = null;
    let imgUrl = null;
  
    if(!fileObject) imgUrl = "https://res.cloudinary.com/dkx3wbimf/image/upload/v1543441487/defaultUser.jpg" 
    else fileBuffer = fileObject.data
    cloudinary.v2.uploader.upload_stream({ resource_type: 'raw' },function (error, result) {
      if(!imgUrl) imgUrl=result.url;
  
      User.insert(new User(login,support.sha512(password, support.serverSalt).passwordHash,fullname,0,imgUrl,bio,false))
        .then( () => {
          console.log(login," is succesfuly registrated");
          res.redirect('/auth/login');
        })
        .catch(err => res.send(err.toString()+" is happened"))
      }).end(fileBuffer);
  
    //res.redirect('../../');
});

router.get('/login', function(req, res) {
  res.render('login', {user : req.user,"menuIn":"currentpage"});
});

router.get('/register', function(req, res) {
  res.render('register', {user : req.user,"menuReg":"currentpage"});
});

router.post('/login', passport.authenticate('local', {
  successRedirect: '/',
  failureRedirect: '/auth/login?error=Wrong+login+or+pssword'
}));

router.get('/logout', support.checkAuth, function(req, res) {
  req.logout();
  console.log('user logged out');
  res.redirect('/');
});

// router.post('/:id',(req, res) => {
//     let id =req.params.id;
//     Bot.delete(id)
//       .then((bot) =>{
//         console.log(bot.name+" is succesfuly deleted");
//         res.redirect('../bots');
//       })
//       .catch(err => res.send(err.toString()+" is happened"))
//   });

//   router.use('/:id',function(req, res) {
//     let id =req.params.id;
//     Bot.getById(id)
//       .then(bot=>{
//         let name = bot.name;
//         let table ={
//           "title":name,
//           "bot": bot
//         };
      
      
//         res.render('bot',table);
//       })
//       .catch(err => res.send(err.toString()+" is happened"))
    
    
//   });
//   router.use('/', function(req, res) {
//     let searchInput = req.query.search ?  req.query.search : "";
//     Bot.getAll()
//       .then(bots =>{
//         let botsListText= [];
//       let botsListText1= [];
//       let botsListText2= [];
//       let i=0;

//       for (let bot of bots){
//         if (bot.name.includes(searchInput)){
//           botsListText.push(bot);
//         }
//       }
//       const pages= Math.ceil(botsListText.length/8);
//       let page =1;
//       if(req.query.page){
//         page=req.query.page;
//       }
//       botsListText.splice(0,(page -1)*8);
//       botsListText.splice(8,9e9);
//       for (let bot of botsListText){
//         if (bot.name.includes(searchInput)){
//         if (i<4){
//           botsListText1.push(bot);
//         }else{
//           botsListText2.push(bot);
//         }
//         i++;}
//       }
//       let pagesArr=[];
//       for(let k = 0;k<pages;k++){
//         pagesArr[k] = k+1;
//       }

//       if(i===0){
//         res.render('bots',{
//           "menuBots":"currentpage",
//           "title":"Telegram bots",
//           "noData": "No data",
//           "searchInput":searchInput
//         });
          
//         }else{
//           res.render('bots',{
//             "menuBots":"currentpage",
//             "title":"Telegram bots",
//             "bots": botsListText1,
//             "bots1":botsListText2,
//             "searchInput":searchInput,
//             "page":page,
//             "pages":pagesArr
//           });
//         }
//       })
//       .catch(err => res.send(err.toString()))
    
//     });

module.exports = router;