const express = require('express');

const router = express.Router();

const User = require("../models/user");
const support = require('../models/crypto');

router.post('/:login',support.checkAdmin,function(req, res) {
  const login =req.params.login;
  User.upgradeToAdmin(login)
    .then(res.redirect("/users"))
});

router.use('/:login',support.checkAuth,function(req, res) {
    const login =req.params.login;
    User.getByLogin(login)
      .populate('collections')
      .exec()
      .then(user => {
        res.render('user',{
          "title":login,
          "user": user,
          curUser : req.user
        })
      })
      .catch(err => res.status(500).send(err.toString()));
    
  
    
  });

  router.use("/",support.checkAdmin,function(req,res){
    let searchInput = req.query.search ?  req.query.search : "";
    User.getAll()
      .then(users => {
        let usersList= [];
        for (let user of users){
          if (user.login.includes(searchInput) || user.fullname.includes(searchInput))
              usersList.push(user);
        }
      const pages= Math.ceil(usersList.length/10);
      let page =1;
      if(req.query.page){
        page=req.query.page;
      }
      usersList.splice(0,(page -1)*10);
      usersList.splice(10,9e9);
      let pagesArr=[];
      for(let k = 0;k<pages;k++){
        pagesArr[k] = k+1;
      }

        res.render("users",{
          "menuUsers":"currentpage",
          "title":"Telegram bots",
          "users": usersList,
          user : req.user,
          "searchInput":searchInput,
          "page":page,
          "pages":pagesArr
        })
      })
      .catch(err => res.status(500).send(err.toString()));
      
  });

module.exports = router;