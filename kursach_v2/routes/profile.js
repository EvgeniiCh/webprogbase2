const express = require('express');

const router = express.Router();

const support = require('../models/crypto');
const User = require("../models/user");
const fileupload = require("express-fileupload");
router.use(fileupload({}));

const config = require('../config');

const cloudinary = require('cloudinary');
cloudinary.config({
    cloud_name: config.cloudinary.cloud_name,
    api_key: config.cloudinary.api_key,
    api_secret: config.cloudinary.api_secret
});

router.post('/changepass',support.checkAuth, function(req, res) {
  let oldPassword = req.body.oldPassword;
  let password = req.body.password;
  let confirmPassword = req.body.confirmPassword;
  if(req.user.password != support.sha512(oldPassword, support.serverSalt).passwordHash){
    res.redirect('/profile/changepass?error=Wrong+old+password');
    return;
  }
  if (password !== confirmPassword) {
    res.redirect('/profile/changepass?error=Passwords+do+not+match');
    return;
}
User.update(req.user.id,new User(req.user.login,support.sha512(password, support.serverSalt).passwordHash,req.user.fullname,req.user.role,req.user.avaUrl,req.user.bio,false,req.user.collections))
        .then( () => {
          console.log(req.user.login," is succesfuly changed password");
          req.logout();
          res.redirect('/auth/login');
        })
        .catch(err => res.send(err.toString()+" is happened"))
  


});

router.post('/update',support.checkAuth, (req, res) => {
  let login = req.body.login;
  let fullname = req.body.fullname;
  let bio =req.body.bio;
if(login != req.user.login)
  if (!User.isUniqueUsername(login))
    if(login != req.user.login) {
      res.redirect('/profile/update?error=Username+already+exists');
      return;
  }
   
    const fileObject = req.files.img;
    let fileBuffer = null;
    let imgUrl = null;
  
    if(!fileObject) imgUrl = req.user.avaUrl; 
    else fileBuffer = fileObject.data
    cloudinary.v2.uploader.upload_stream({ resource_type: 'raw' },function (error, result) {
      if(!imgUrl) imgUrl=result.url;
  
      User.update(req.user.id,new User(login,req.user.password,fullname,req.user.role,imgUrl,bio,false,req.user.collections))
        .then( () => {
          console.log(login," is succesfuly updated");
          req.logout();
          res.redirect('/auth/login');
        })
        .catch(err => res.send(err.toString()+" is happened"))
      }).end(fileBuffer);
  
    //res.redirect('../../');
});

router.get('/update',support.checkAuth, function(req, res) {
    res.render('updateusr', {user : req.user});
  });

  router.get('/changepass',support.checkAuth, function(req, res) {
    res.render('changepass', {user : req.user});
  });

router.use('/',support.checkAuth,(req,res) =>{
  //console.log(req.user);    
  User.getById(req.user.id)
      .populate('collections')
      .exec()
      .then(user => {
          res.render('profile',{
            "title":user.login,
            "user": user,
            curUser : req.user
          })
        
      })
      .catch(err => res.status(500).send(err.toString()));
  })
  

  module.exports = router;