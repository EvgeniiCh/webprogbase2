const express = require('express');
const router = express.Router();
const passport = require('../models/passport.js');
const User = require("../models/user");
const Collection = require("../models/collections");

const Bot = require("../models/bot");
const support = require('../models/crypto');
const fileupload = require("express-fileupload");
router.use(fileupload({}));

const config = require('../config');

const cloudinary = require('cloudinary');
cloudinary.config({
    cloud_name: config.cloudinary.cloud_name,
    api_key: config.cloudinary.api_key,
    api_secret: config.cloudinary.api_secret
});
/************************************BOT****************************************************************** */

router.post('/bots/new',passport.authenticate('basic', {session: false}), 
(req, res) => {
  //console.log(req.body);
  let name=req.body.name;
  let url =req.body.url;
  let fileObject = null;
  if(req.files)
    fileObject = req.files.img;
  let fileBuffer = null;

  if(!url) fileBuffer = fileObject.data;
  
  
  let price = req.body.price;
  let bio =req.body.bio;
  let botUrl = req.body.botUrl;
  if(!name||!price||!bio||!botUrl){
    res.json(new Error("Some fields are emty").message);
    return;
  }

  cloudinary.v2.uploader.upload_stream({ resource_type: 'raw' },function (error, result) {
    if(!url) url=result.url;

    Bot.insert(new Bot(name, price, url, bio, botUrl,req.user.id,0))
      .then( (bot) => {
        console.log(name," is succesfuly added");
        res.status(201).json(bot);
      })
      .catch(err => {
        console.log(err.message);
        res.sendStatus(500);
        req.next();
    }); 
    }).end(fileBuffer);
}
);

router.delete('/bots/:id',passport.authenticate('basic', {session: false}), 
(req, res) => {
  let id =req.params.id;

  Bot.delete(id)
      .then((bot) =>{
        if(!bot){
          res.sendStatus(404);
          return;
        }
        console.log(bot.name+" is succesfuly deleted");
        res.status(200).json(bot);
      })
      .catch(err => {
        console.log(err.message);
        res.json(err.message);
        req.next();
    }); 
});

router.post('/bots/:id/update',passport.authenticate('basic', {session: false}), 
(req, res) => {
  let id =req.params.id;
  let name=req.body.name;
    let url =req.body.url;
    let price = req.body.price;
    let bio =req.body.bio;
    let botUrl = req.body.botUrl;
  Bot.getById(id)
  .populate("owner")
  .exec()
   .then(bot=>{
    if(!bot){
      res.sendStatus(404);
      return;
    }
    if(!name)
      name = bot.name;
    if(!price)
      price =bot.price;
    if(!bio)
      bio = bot.bio;
    if(!botUrl)
      botUrl = bot.botUrl;
    let fileObject = null;
  if(req.files)
    fileObject = req.files.img;
  let fileBuffer = null;
    if(!url &&fileObject) fileBuffer = fileObject.data;
    if(!url &&!fileObject) url= bot.avaUrl;
    cloudinary.v2.uploader.upload_stream({ resource_type: 'raw' },function (error, result) {
      if(!url) url=result.url;
  
      Bot.update(id,new Bot(name, price, url, bio, botUrl,bot.owner.id,bot.populated))
        .then( (bot) => {
          if(!bot){
            res.sendStatus(404);
            return;
          }
          console.log(name," is succesfuly updated");
          res.status(200).json(bot);
        })
        .catch(err => {
          console.log(err.message);
          res.sendStatus(500);
          req.next();
      }); 
      }).end(fileBuffer);
   })
  

  
});



router.get('/bots/:id',passport.authenticate('basic', {session: false}), 
(req, res) => {
  let id =req.params.id;
    Bot.getById(id)
      .then(bot=>{
        if(!bot){
          res.sendStatus(404);
          return;
        }
          
        res.json(bot);
      })
      .catch(err => {
        console.log(err.message);
        res.sendStatus(404);
        req.next();
    });      
});

router.use('/bots',passport.authenticate('basic', {session: false}), 
(req, res) => {
  let searchInput = req.query.search ?  req.query.search : "";
  Bot.getAll()
    .then(bots =>{
      let botsListText= [];
    

    for (let bot of bots){
      if (bot.name.includes(searchInput)){
        botsListText.push(bot);
      }
    }
    const pageSize = 4;
    const pages= Math.ceil(botsListText.length/pageSize);
    let page =1;
    if(req.query.page){
      page=req.query.page;
    }
    botsListText.splice(0,(page -1)*pageSize);
    botsListText.splice(pageSize,9e9);
    

    res.status(200).json(botsListText);})
    .catch(err => {
      console.log(err.message);
      res.sendStatus(404);
      req.next();
  });        
  
  });



/************************************BOT****************************************************************** end*/


/************************************COLLECTION****************************************************************** */



router.post('/collections/new', passport.authenticate('basic', {session: false}), 
(req, res) => {
  if (!req.user.role){
    res.sendStatus(403);
    return;
  }
  let name=req.body.name;
  let bots=req.body.bots;
  let user=req.body.user;
  //console.log(name+" "+ bots+" "+ user);
  if(!name||!bots||!user){
    res.json(new Error("Some fields are emty").message);
    return;
  }
  
  Promise.resolve()
    .then(()=>{
      for (bot of bots){
        Bot.getById(bot)
      }
    })
    .then(
      Collection.insert(new Collection(name,bots))
    .then((collection) =>User.addCollectionById(user,collection.id)
    .then((collection) =>{res.status(201).json(collection)})
    .catch(err => {
      console.log(err.message);
      res.sendStatus(500);
      req.next();
  })))

  
});

router.post('/collections/:id/update', passport.authenticate('basic', {session: false}), 
(req, res) => {
  if (!req.user.role){
    res.sendStatus(403);
    return;
  }
  const id =req.params.id;
  let name=req.body.name;
  let bots=req.body.bots;
  

  Collection.getById(id)
    .then(collection => {
      if(!collection){
        res.sendStatus(404);
        return;
      }
      if(!name)
       name =collection.name;
      if (!bots)
        bots = collection.bots;
      Promise.all(bots)
        .then((bot)=>{
          bot=Bot.getById(bot);
        })
        .then(()=>{
          collection =new Collection(name,bots);

      Collection.update(id,collection)
          
      res.status(200).json(collection)
        })
    } )
    .catch(err => {
      console.log(err.message);
      res.sendStatus(500);
      req.next();
  })

  
});

router.delete('/collections/:id',passport.authenticate('basic', {session: false}), 
(req, res) => {
  const id =req.params.id;
  if (!req.user.role){
    res.sendStatus(403);
    return;
  }
  Collection.delete(id)
      .then((collection) =>{
        
        console.log("Collection is succesfuly deleted");
        res.status(200).json(collection);
      })
      .catch(err => {
        console.log(err.message);
        res.sendStatus(500);
        req.next();
    })
});

router.get('/collections/:id',passport.authenticate('basic', {session: false}), 
(req, res) => {
  
  const id =req.params.id;
  
  Collection.getById(id)
  .populate('bots')
  .exec()
      .then(collection => {
        if(!collection) {
          res.json(new Error("There is no such collection").message);
          return;
        }
                
        res.json(collection);
      })
      .catch(err => {
        console.log(err.message);
        res.sendStatus(404);
        req.next();
    });
});

router.use('/collections',passport.authenticate('basic', {session: false}), 
(req, res) => {
  let searchInput = req.query.search ?  req.query.search : "";
  Collection.getAll()
  .populate('bots')
  .exec()
    .then(collections =>{
      let collectionsListText= [];

    for (let collection of collections){
      if (collection.name.includes(searchInput)){
        collectionsListText.push(collection);
      }
    }
    const pageSize = 4;
    const pages= Math.ceil(collectionsListText.length/pageSize);
    let page =1;
    if(req.query.page){
      page=req.query.page;
    }
    collectionsListText.splice(0,(page -1)*pageSize);
    collectionsListText.splice(pageSize,9e9);
    res.json(collectionsListText);})
    .catch(err => {
      console.log(err.message);
      res.sendStatus(404);
      req.next();
  });        
  
});

/************************************COLLECTION****************************************************************** end*/


/**********************************USER**********************************************************8 */
router.post('/users/new', 
  (req, res) => {
  let login = req.body.login;
  let password = req.body.password;
  let fullname = req.body.fullname;
  let bio =req.body.bio;
  let imgUrl = req.body.avaUrl;
  if(!login||!password||!fullname||!bio){
    res.json(new Error("Some fields are emty").message);
    return;
  }
  //console.log(req.body);
  if (!User.isUniqueUsername(login)) {
    res.json(new Error("Username already exists").message);
    return;
  }
    let fileObject = null;
    if (req.files)
      fileObject = req.files.img;
    let fileBuffer = null;
  
    if(!fileObject) imgUrl = "https://res.cloudinary.com/dkx3wbimf/image/upload/v1543441487/defaultUser.jpg" 
    else fileBuffer = fileObject.data
    cloudinary.v2.uploader.upload_stream({ resource_type: 'raw' },function (error, result) {
      if(!imgUrl) imgUrl=result.url;
  
      User.insert(new User(login,support.sha512(password, support.serverSalt).passwordHash,fullname,0,imgUrl,bio,false))
        .then( (user) => {
          console.log(login," is succesfuly registrated");
          res.status(201).json(user);
        })
        .catch(err => {
          console.log(err.message);
          res.sendStatus(500);
          req.next();
      });
      }).end(fileBuffer);
  });


router.post('/users/:login/update',passport.authenticate('basic', {session: false}), 
(req, res) => {
  const curLogin =req.params.login;
      if (curLogin != req.user.login){
        res.sendStatus(403);
        return;
      }
      let login =req.body.login;
      let oldPass =req.body.oldPass;
      if(req.user.password != support.sha512(oldPass, support.serverSalt).passwordHash){
        res.json(new Error("Wrong old password").message);
        return;
      }
      let newPass =req.body.newPass;
      let fullname =req.body.fullname;
      let bio =req.body.bio;
      let imgUrl =req.body.avaUrl;
      if (!login)
        login = req.user.login;
      if (!newPass)
        newPass = oldPass;
      if (!fullname)
        fullname = req.user.fullname;
      if (!bio)
        bio = req.user.bio;
      if(login != req.user.login)
        if (!User.isUniqueUsername(login)){
          res.json(new Error("Username already exists").message);
          return;
        }
      let fileObject = null;
      if(req.files)
        fileObject = req.files.img;
      let fileBuffer = null;
      console.log(newPass+ "" + fullname);
      if(!fileObject) imgUrl = req.user.avaUrl; 
      else fileBuffer = fileObject.data
      cloudinary.v2.uploader.upload_stream({ resource_type: 'raw' },function (error, result) {
        if(!imgUrl) imgUrl=result.url;
    
        User.update(req.user.id,new User(login,support.sha512(newPass, support.serverSalt).passwordHash,fullname,req.user.role,imgUrl,bio,false,req.user.collections))
          .then( (user) => {
            console.log(login," is succesfuly updated");
            res.status(200).json(user);
          })
          .catch(err => {
            console.log(err.message);
            res.sendStatus(500);
            req.next();
        });
        }).end(fileBuffer);
  });

router.delete('/users/:login',passport.authenticate('basic', {session: false}), 
  (req, res) => {
    
    const login =req.params.login;
    if (!req.user.role && (login != req.user.login)){
      res.sendStatus(403);
      return;
    }
    
    User.deleteByLogin(login)
      .then((user) =>{
        console.log(user.name+" is succesfuly deleted");
        res.status(200).json(user);
      })
      .catch(err => {
        console.log(err.message);
        res.sendStatus(500);
        req.next();
    });
  });

router.get('/users/:login',passport.authenticate('basic', {session: false}), 
(req, res) => {
  
  const login =req.params.login;
  if (!req.user.role && (login != req.user.login)){
    res.sendStatus(403);
    return;
  }
  
  User.getByLogin(login)
      .populate('collections')
      .exec()
      .then(user => {
        if(!user) 
                res.json(new Error("There is no such user").message);
        res.json(user);
      })
      .catch(err => {
        console.log(err.message);
        res.sendStatus(404);
        req.next();
    });
});



  router.use('/users',passport.authenticate('basic', {session: false}), 
  (req, res) => {
    if (!req.user.role){
      res.sendStatus(403);
      return;
    }
    let searchInput = req.query.search ?  req.query.search : "";
    User.getAll()
    .populate("collections")
    .exec()
      .then(users =>{
        let usersListText= [];

      for (let user of users){
        if (user.fullname.includes(searchInput)){
          usersListText.push(user);
        }
      }
      const pageSize = 4;
      const pages= Math.ceil(usersListText.length/pageSize);
      let page =1;
      if(req.query.page){
        page=req.query.page;
      }
      usersListText.splice(0,(page -1)*pageSize);
      usersListText.splice(pageSize,9e9);
      res.json(usersListText);})
      .catch(err => {
        console.log(err.message);
        res.sendStatus(404);
        req.next();
    });        
    
  });
  /**************************************USER************************************************* end*/

  router.get('/me', passport.authenticate('basic', {session: false}), 
    (req, res) => {
        res.json(req.user);
    }
);

  router.use('/',(req, res) => {
    res.json(JSON.stringify());
  });

module.exports = router;