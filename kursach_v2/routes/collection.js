const express = require('express');

const router = express.Router();

const Collection = require("../models/collections");

const Bot = require("../models/bot");

const User = require("../models/user");
const support = require('../models/crypto');

router.get('/newCollection', support.checkAuth,function(req, res) {
  Bot.getAll()
    .populate("owner")
    .exec()
    .then(bots =>{
      let botList = [];
      for (let bot of bots){
          if (req.user.role || req.user.id === bot.owner.id || bot.publicated){
            botList.push(bot);
        }
      }
      
      res.render('newcollection',{"title":"New collection", "bots":botList ,user : req.user,"menuNewColl":"currentpage"})
    })
  
});

router.post('/newCollection', support.checkAuth,function(req, res) {
  let name=req.body.name;
  let bots=req.body.bots;
  let shared = req.body.shared;
  if (shared ==='on')
    shared = 1
  else shared =0;
  
  
  Promise.resolve()
    .then(()=>{
      for (bot of bots){
        Bot.getById(bot)
      }
    })
    .then(
      Collection.insert(new Collection(name,bots,req.user.id,shared))
    .then((collection) =>User.addCollectionById(req.user.id,collection.id)
    .then(res.redirect('/collections'))
    .catch(err => res.send(err.toString()+" is happened"))))
  
  


  //res.redirect('/collections');
  
});

router.post('/:id/update', support.checkAuth,function(req, res) {
  let id =req.params.id;
  Collection.getById(id)
  .populate("owner")
    .exec()
    .then(collection =>{
      if (!req.user.role && req.user.id != collection.owner.id){
        res.sendStatus(403);
        return;
      }
      let name=req.body.name;
  let bots=req.body.bots;
  let shared = req.body.shared;
  if (shared ==='on')
    shared = 1
  else shared =0;
  
  
  Promise.resolve()
    .then(()=>{
      for (bot of bots){
        Bot.getById(bot)
      }
    })
    .then(
      Collection.update(id,new Collection(name,bots,req.user.id,shared))
    .then(res.redirect('/collections')))
    }).catch(err => res.send(err.toString()+" is happened"))
  
    
})

router.get('/:id/update', support.checkAuth,function(req, res) {
  let id =req.params.id;
  Collection.getById(id)
    .populate("owner")
    .exec()
   .then(collection =>{
    if (!req.user.role && req.user.id != collection.owner.id){
      res.sendStatus(403);
      return;
    }
    Bot.getAll()
    .populate("owner")
    .exec()
    .then(bots =>{
      let botList = [];
      for (let bot of bots){
          if (req.user.role || req.user.id === bot.owner.id || bot.publicated){
            botList.push(bot);
        }
      }
      
      res.render('upcollection',{"title":"Update collection", "bots":botList ,user : req.user,"name": collection.name})
    })
   })
  
  
});

router.post('/:id',support.checkAuth,function(req, res) {
    let id =req.params.id;
    Collection.getById(id)
      .populate("owner")
      .exec()
      .then(collection=>{
        if (!req.user.role && req.user.id != collection.owner.id){
          res.sendStatus(403);
          return;
        }
        Collection.delete(id)
      .then(() =>{
        console.log("Collection is succesfuly deleted");
        res.redirect('/collections');
      })
      })

    
      .catch(err => res.send(err.toString()+" is happened"))
  });

  router.use('/:id',support.checkAuth,function(req, res) {
    let id =req.params.id;
    let botsListText= [];
    Collection.getById(id)
      .populate("bots")
      .populate("owner")
      .exec()
      .then(collection=>{
        if (!req.user.role && req.user.id != collection.owner.id && !collection.shared){
          res.sendStatus(403);
          return;
        }

        for (let bot of collection.bots){
          if (req.user.role || req.user.id.toString() === bot.owner.toString() || bot.publicated){
             botsListText.push(bot);
          }
        }
          //console.log(botsListText);
        let botsListText1= [];
      let botsListText2= [];
      let i=0;
      const pages= Math.ceil(botsListText.length/8);
      let page =1;
      if(req.query.page){
        page=req.query.page;
      }
      botsListText.splice(0,(page -1)*8);
      botsListText.splice(8,9e9);
      for (let bot of botsListText){
        if (i<4){
          botsListText1.push(bot);
        }else{
          botsListText2.push(bot);
        }
        i++;}
      let pagesArr=[];
      for(let k = 0;k<pages;k++){
        pagesArr[k] = k+1;
      }
      let rights =0;
      if (req.user.role)
          rights =1;
      if (collection.owner)
          if(req.user.id === collection.owner.id)
            rights =1;
      
          
      
          res.render('collection',{
            "title":"Telegram bots",
            "bots": botsListText1,
            "bots1":botsListText2,
            "page":page,
            "pages":pagesArr,
            "collection": collection,
            "rights":rights,
            user : req.user
          });


        
        
      
      
      })
      .catch(err => res.send(err.toString()+" is happened"))
    
    
  });

  router.use("/",support.checkAuth,function(req,res){
    let searchInput = req.query.search ?  req.query.search : "";
    Collection.getAll()
      .populate("owner")
      .exec()
      .then(colls => {
        let collList = [];
      for (let coll of colls){
          if (req.user.role || req.user.id === coll.owner.id || coll.shared){
            if (coll.name.includes(searchInput) || coll.owner.login.includes(searchInput))
              collList.push(coll);
        }
      }
      const pages= Math.ceil(collList.length/10);
      let page =1;
      if(req.query.page){
        page=req.query.page;
      }
      collList.splice(0,(page -1)*10);
      collList.splice(10,9e9);
      let pagesArr=[];
      for(let k = 0;k<pages;k++){
        pagesArr[k] = k+1;
      }


        res.render("collections",{
          "menuCollections":"currentpage",
          "title":"Bot Collections",
          "collections": collList,
          "searchInput":searchInput,
          "page":page,
          "pages":pagesArr,
          user : req.user
        })
      })
      .catch(err => res.status(500).send(err.toString()));
      
  });






module.exports = router;