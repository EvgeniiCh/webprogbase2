const express = require('express');

const router = express.Router();

const Bot = require("../models/bot");

const support = require('../models/crypto');

//const path = require('path');

const fileupload = require("express-fileupload");
router.use(fileupload({}));



const config = require('../config');

const cloudinary = require('cloudinary');
cloudinary.config({
    cloud_name: config.cloudinary.cloud_name,
    api_key: config.cloudinary.api_key,
    api_secret: config.cloudinary.api_secret
});
router.post('/:id/hide',support.checkAuth, function(req, res) {
  let id =req.params.id;
  Bot.getById(id)
    .populate("owner")
    .exec()
    .then(bot =>{
      
      if (!req.user.role && req.user.id != bot.owner.id){
        res.sendStatus(403);
        return;
      }else{
        Bot.hide(id)
        .then(console.log(bot.name + " now is hidden"))
        .then(res.redirect("/bots/"+id))
      }}).catch(err => res.send(err.toString()+" is happened"))
});
router.post('/:id/publicate',support.checkAuth, function(req, res) {
  let id =req.params.id;
  Bot.getById(id)
    .populate("owner")
    .exec()
    .then(bot =>{
      
      if (!req.user.role && req.user.id != bot.owner.id){
        res.sendStatus(403);
        return;
      }else{
        Bot.publicate(id)
        .then(console.log(bot.name + " now is publicated"))
        .then(res.redirect("/bots/"+id))
      }}).catch(err => res.send(err.toString()+" is happened"))
});

router.post('/:id/update',support.checkAuth, function(req, res) {
  let id =req.params.id;
  //console.log(req.body);
  Bot.getById(id)
    .populate("owner")
    .exec()
    .then(bot =>{
      
      if (!req.user.role && req.user.id != bot.owner.id){
        res.sendStatus(403);
        return;
      }else{
        let name=req.body.name;
        let url =req.body.url;
        const fileObject = req.files.img;
        let fileBuffer = null;
      
        if(!url) fileBuffer = fileObject.data;
        
        
        let price = req.body.price;
        let bio =req.body.bio;
        let botUrl = req.body.botUrl;
      
        cloudinary.v2.uploader.upload_stream({ resource_type: 'raw' },function (error, result) {
          if(!url) url=result.url;
      
          Bot.update(id,new Bot(name, price, url, bio, botUrl,bot.owner.id,bot.publicated))
            .then( () => {
              console.log(name," is succesfuly updated");
              res.redirect('../../bots');
            })
            
          }).end(fileBuffer);
      }}).catch(err => res.send(err.toString()+" is happened"))

}
  
);
router.get('/:id/update',support.checkAuth, function(req, res) {
  let id =req.params.id;
    Bot.getById(id)
    .populate("owner")
    .exec()
      .then(bot=>{
        if (!req.user.role && req.user.id != bot.owner.id){
          res.sendStatus(403);
          return;
        }else{
          res.render('updatebot',{"title":"Update bot",user : req.user,bot:bot});
        }
        
      })
      .catch(err => res.send(err.toString()+" is happened"))
  
});

router.post('/newbot',support.checkAuth, (req, res) => {
  //console.log(req.body);
  let name=req.body.name;
  let url =req.body.url;
  const fileObject = req.files.img;
  let fileBuffer = null;

  if(!url) fileBuffer = fileObject.data;
  
  
  let price = req.body.price;
  let bio =req.body.bio;
  let botUrl = req.body.botUrl;

  cloudinary.v2.uploader.upload_stream({ resource_type: 'raw' },function (error, result) {
    if(!url) url=result.url;

    Bot.insert(new Bot(name, price, url, bio, botUrl,req.user.id,0))
      .then( () => {
        console.log(name," is succesfuly added");
        res.redirect('../bots');
      })
      .catch(err => res.send(err.toString()+" is happened"))
    }).end(fileBuffer);
}
);

router.use('/newbot',support.checkAuth, function(req, res) {
  res.render('newbot',{"title":"New bot",user : req.user,"menuNewBot":"currentpage"});
});

router.post('/:id',support.checkAuth,(req, res) => {
    let id =req.params.id;
    Bot.getById(id)
    .populate("owner")
    .exec()
    .then(bot =>{
      if (!req.user.role && req.user.id != bot.owner.id){
        res.sendStatus(403);
        return;
      }else{
        Bot.delete(id)
      .then(() =>{
        console.log(bot.name+" is succesfuly deleted");
        res.redirect('../bots');
      })
      }
    })
    
      .catch(err => res.send(err.toString()+" is happened"))
  });

  router.use('/:id',support.checkAuth,function(req, res) {
    let id =req.params.id;
    Bot.getById(id)
    .populate("owner")
    .exec()
      .then(bot=>{
        if (!req.user.role && req.user.id != bot.owner.id && !bot.publicated){
        res.sendStatus(403);
        return;
      }
        let rights =0;
        if (req.user.role)
          rights =1;
        if (bot.owner)
          if(req.user.id === bot.owner.id)
            rights =1;
        //console.log(bot.owner.id)
        let name = bot.name;
        let table ={
          "title":name,
          "bot": bot,
          "rights": rights,
          user : req.user
        };
      
      
        res.render('bot',table);
      })
      .catch(err => res.send(err.toString()+" is happened"))
    
    
  });
  router.use('/',support.checkAuth, function(req, res) {
    let searchInput = req.query.search ?  req.query.search : "";
    Bot.getAll()
      .populate("owner")
      .exec()
      .then(bots =>{
        let botsListText= [];
      let botsListText1= [];
      let botsListText2= [];
      let i=0;

      for (let bot of bots){
        if (bot.name.includes(searchInput))
          if (req.user.role || req.user.id === bot.owner.id || bot.publicated){
            botsListText.push(bot);
        }
      }
      const pages= Math.ceil(botsListText.length/8);
      let page =1;
      if(req.query.page){
        page=req.query.page;
      }
      botsListText.splice(0,(page -1)*8);
      botsListText.splice(8,9e9);
      for (let bot of botsListText){
        if (bot.name.includes(searchInput)){
        if (i<4){
          botsListText1.push(bot);
        }else{
          botsListText2.push(bot);
        }
        i++;}
      }
      let pagesArr=[];
      for(let k = 0;k<pages;k++){
        pagesArr[k] = k+1;
      }

      if(i===0){
        res.render('bots',{
          "menuBots":"currentpage",
          "title":"Telegram bots",
          "noData": "No data",
          "searchInput":searchInput,
          user : req.user
        });
          
        }else{
          res.render('bots',{
            "menuBots":"currentpage",
            "title":"Telegram bots",
            "bots": botsListText1,
            "bots1":botsListText2,
            "searchInput":searchInput,
            "page":page,
            "pages":pagesArr,
            user : req.user
          });
        }
      })
      .catch(err => res.send(err.toString()))
    
    });

module.exports = router;