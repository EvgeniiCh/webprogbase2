function confirmation() {
    return confirm("Are you sure?");
}

function validateForm(form) {

    if(form.password.value == ""){
        alert("Error: Empty field with password!");
      form.password.focus();
      return false;
    }
    if(form.password.value != form.confirmPassword.value) {
        alert("Error: Passwords do not match!");
      form.password.focus();
      return false;
    }
}