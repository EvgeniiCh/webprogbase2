
const Collection = require("./collections");
const mongoose = require('mongoose');
mongoose.set('useFindAndModify', false)

const BotSchema = new mongoose.Schema({
    name: { type: String, required: true },
    price: { type: Number, required: true },
    avaUrl: { type: String },
    bio: { type: String },
    url: { type: String, required: true },
    timeAdd: { type: String, default: new Date().toISOString() },
    owner: { type: mongoose.Schema.Types.ObjectId,ref : "User"},
    publicated: { type : Number }
})

const BotModel = mongoose.model('Bot',BotSchema)

class Bot {
    constructor (name,price,avaUrl,bio,url,owner,publicated){
        this.name = name;
        this.price = parseFloat(price);
        this.avaUrl = avaUrl;
        this.bio = bio;
        this.url = url;
        this.owner = owner;
        this.publicated = publicated;

    }

    static insert(bot){
        
        return new BotModel(bot).save();
    }

    static update(id,bot){
        
        return BotModel.findOneAndUpdate({ _id : id},bot);
    }

    static delete(id){
        return Collection.deleteBotById(id)
            .then(() => BotModel.findByIdAndDelete(id))
    }

    static getAll(){
        return BotModel.find();
    }

    static getById(id){
        return BotModel.findById(id);
        
    }

    static getByName(name){
        return BotModel.findOne({ name : name});
        
    }
    static publicate(id){
        return BotModel.updateOne({_id : id},{ $set: { publicated: 1 } })
    }
    static hide(id){
        return BotModel.updateOne({_id : id},{ $set: { publicated: 0 } })
    }
}

module.exports = Bot;

/*module.exports = {
    getById:function(id,callback) {
        read((data)=>{
            let list = data.items;
            let bot= list.find(x=> x.id === id);
            callback(bot); 
        });
        
    },
    getAll:function(callback){
        read((data)=>{
            let bots=data.items;
            //console.log(bots);
            callback(bots);
        });    
    },
    delete:function(id,callback) {
        /*const newBots={
            "nextId": Bots.nextId-1,
            "items": [
                
            ]
        };
        for (let bot of Bots.items){
            if (bot.id <id){
                newBots.items.push(bot);
            }
            if (bot.id >id){
                bot.id--;
                newBots.items.push(bot);
            }
        }* /
        read((data)=>{
            for(let bot of data.items ){
                if( bot.id === id){
                    data.items.splice(data.items.indexOf(bot), 1);
                    //Bots.nextId--;
                }
                //Bots.items.find(x=> x.id >id).id--;
            }
            
            fs.writeFile('./data/bots.json',JSON.stringify(data,null,4) ,(err)=>
            {
                callback(err);
            });
            //return deleteBot;
        });
        
    },
    update:function(id,name,url,price,bio,callback) {
        read((data)=>{
            for (let bot of data.items)  {
                if(bot.id === id){
                    bot.name=name;
                    bot.url=url;
                    bot.price=parseFloat(price);
                    bot.timeAdd= new Date().toISOString();
                    bot.bio=bio;
                }
            }  
            fs.writeFile('./data/bots.json',JSON.stringify(data,null,4),(err)=>
            {
                callback(err);
            } );
        });
        
    },
    insert:function(name,url,price,bio,callback) {
        read((data)=>{
            const id = data.nextId;
        let bot = {
            "id":  id,
            "name": name,
            "price": parseFloat(price),
            "timeAdd": new Date().toISOString(),
            "url": url,
            "bio":bio
        };
        data.nextId++;
        data.items.push(bot) ;
        fs.writeFile('./data/bots.json',JSON.stringify(data,null,4) ,(err)=>
        {
            callback(err);
        }
         );
        });
        
    }
    
    
    
};*/