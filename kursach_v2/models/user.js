/*function User(id,login,fullname){
    this.id=id;
    this.login=login;
    this.fullname=fullname;
};*/

const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
    login: { type: String, required: true },
    fullname: { type: String },
    password: { type: String },
    role: { type : Number },
    avaUrl: { type: String },
    bio: { type: String },
    isDisabled: { type: Boolean },
    created: { type: String, default: new Date().toISOString() },
    collections: [{ type: mongoose.Schema.Types.ObjectId,ref : "Collection"}]
})

const UserModel = mongoose.model('User',UserSchema)

class User {
    constructor (login,password,fullname,role,avaUrl,bio,isDisabled,collections){
        this.login = login;
        this.password = password;
        this.fullname = fullname;
        this.role = role;
        this.avaUrl = avaUrl;
        this.bio = bio;
        this.isDisabled = isDisabled;
        this.collections = collections;

    }


 /*
    static getById(id,callback){
        read((data)=>{
            let user = data.items.find(x=> x.id === id);
        
        callback(user); 
        });
        
    }

    static getByLogin(login,callback) 
    {
        read((data)=>{
            let user = data.items.find(x=> x.login === login);
        
        callback(user); 
        });
    }*/
    static deleteByLogin(login){
        return UserModel.findOneAndDelete({login : login});
    }

    static update(id,user){
        
        return UserModel.findOneAndUpdate({ _id : id},user);
    }

    static getById(id){
        return UserModel.findOne({ _id : id});
        
    }
    static getByLogin(login){
        return UserModel.findOne({ login : login});
        
    }
    static addEmptyUser(){
        return new UserModel(new User("xxx")).save();
    }

    static getAll(){
        return UserModel.find();
    }
    
    static insert(user){
        
        return new UserModel(user).save();
    }
    static deleteCollectionById(id){
        return UserModel.updateMany({}, { $pull: { collections:  id} });
    }
    static addCollectionById(userId,collId){
        return UserModel.updateOne({"_id": userId}, { $push: { collections:  collId} });
    }
    static isUniqueUsername(login){
        return UserModel.findOne({ login : login});
    }
    static getUserByLoginAndPassword(login, password) {
        
            return UserModel.findOne({ login : login, password: password});
        
    }
    static upgradeToAdmin(login){
        return UserModel.updateOne({login : login},{ $set: { role: 1 } })
    }

    
}
module.exports = User;