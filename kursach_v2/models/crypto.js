const crypto = require('crypto');

const serverSalt = "ggwp";

function sha512(password, salt) {
    const hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    const value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
};

function checkAuth(req, res, next) {
    if (!req.user) return res.sendStatus(401); 
    next();
}

function checkAdmin(req, res, next) {
    if (req.user.role !== 1) return res.sendStatus(403); 
    next();
}

module.exports = {
    serverSalt : serverSalt,
    sha512 : sha512,
    checkAdmin : checkAdmin,
    checkAuth : checkAuth
};