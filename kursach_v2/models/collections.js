
const mongoose = require('mongoose')
const User = require("./user");

const CollectionSchema = new mongoose.Schema({
    name: { type: String, required: true },
    bots: [{ type: mongoose.Schema.Types.ObjectId,ref : "Bot"}],
    timeAdd: { type: String, default: new Date().toISOString() },
    owner: { type: mongoose.Schema.Types.ObjectId,ref : "User"},
    shared: { type : Number }
})

const CollectionModel = mongoose.model('Collection',CollectionSchema)

class Collection {
    constructor (name,bots,owner,shared){
        this.name = name;
        this.bots = bots;
        this.owner = owner;
        this.shared = shared;


    }

    static insert(collection){
        
        return new CollectionModel(collection).save();
    }

    static update(id,collection){
        //console.log(id);
        //console.log(collection);
        return CollectionModel.findOneAndUpdate({ _id:id },collection);
    }

    static delete(id){
        
        return CollectionModel.findByIdAndDelete(id)
            .then(() => User.deleteCollectionById(id))
    }

    static deleteBotById(id){
        return CollectionModel.updateMany({}, { $pull: { bots:  id} });
    }

    static getAll(){
        return CollectionModel.find();
    }

    static getById(id){
        return CollectionModel.findById(id);
        
    }
    
}

module.exports = Collection;

