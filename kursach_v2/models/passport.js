const User = require('./user.js');


const LocalStrategy = require('passport-local').Strategy;
const passport = require('passport');
const BasicStrategy = require('passport-http').BasicStrategy;
const support = require('./crypto');

passport.serializeUser(function(user, done) {
    //console.log(user.login+" 10 pasport.js");
    console.log(user.login +" connected");
    done(null, user._id);

});


passport.deserializeUser(function(id, done) {
    //console.log(id+" passport.js 17");
    User.getById(id)
    .then(user => {
        
        done(null, user);
    })
    .catch(err => {
        //console.log(err.message);
        done(err, null);
    }); 
});

passport.use(new BasicStrategy(
    function (username, password, done) {
        let hash = support.sha512(password, support.serverSalt).passwordHash;
       // console.log(username, password);
        User.getUserByLoginAndPassword(username, hash)
            .then(user => {
                if(user){
                    //console.log(user.login+" connected");
                }
                
                done(null, user);
            })
            .catch(err => {
                console.log(err.message);
                done(err, null);
            });
    }
));


passport.use(new LocalStrategy(
    function (username, password, done) {
        let hash = support.sha512(password, support.serverSalt).passwordHash;
        //console.log(username, hash);
        User.getUserByLoginAndPassword(username, hash)
            .then(user => {
                if(user){
                    //console.log(user.login+" connected");
                }
                
                done(null, user);
            })
            .catch(err => {
                console.log(err.message);
                done(err, null);
            });
    }
));



module.exports = passport;